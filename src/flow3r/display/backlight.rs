use hal::gpio::{Gpio46, GpioPin, Unknown};
use hal::ledc::channel::Channel;
use hal::ledc::timer::Timer;
use hal::ledc::{LSGlobalClkSource, LowSpeed, LEDC};
use hal::prelude::*;
use static_cell::StaticCell;

pub type Backlight = Channel<'static, LowSpeed, GpioPin<Unknown, 46>>;

static LEDC: StaticCell<LEDC<'static>> = StaticCell::new();
static LSTIMER0: StaticCell<Timer<'static, LowSpeed>> = StaticCell::new();

pub fn init_backlight(ledc: LEDC<'static>, bl: Gpio46<Unknown>) -> Backlight {
    let ledc = LEDC.init(ledc);

    ledc.set_global_slow_clock(LSGlobalClkSource::APBClk);

    let lstimer0 = LSTIMER0.init(ledc.get_timer::<LowSpeed>(hal::ledc::timer::Number::Timer0));
    lstimer0
        .configure(hal::ledc::timer::config::Config {
            duty: hal::ledc::timer::config::Duty::Duty5Bit,
            clock_source: hal::ledc::timer::LSClockSource::APBClk,
            frequency: 24u32.kHz(),
        })
        .unwrap();

    let mut channel0 = ledc.get_channel(hal::ledc::channel::Number::Channel0, bl);
    channel0
        .configure(hal::ledc::channel::config::Config {
            timer: lstimer0,
            duty_pct: 0,
            pin_config: hal::ledc::channel::config::PinConfig::PushPull,
        })
        .unwrap();

    channel0
}
