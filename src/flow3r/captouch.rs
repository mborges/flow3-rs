use ad7147::{
    device::DecimationFactor,
    stage::{CapInput, CdcInput, InputConnection, StageSensitivity, ThresholdSensitivity, PeakDetect},
    Ad7147, DeviceConfiguration, Initialized, StageConfiguration,
};
use embassy_futures::select::{select, Either};
use embassy_sync::{
    blocking_mutex::raw::CriticalSectionRawMutex,
    mutex::Mutex,
    pubsub::{PubSubChannel, Subscriber},
};
use embassy_time::Delay;
use embedded_hal_async::digital::Wait;
use esp_println::println;
use hal::{
    gpio::{Gpio15, Gpio16, Unknown},
    i2c::I2C,
    peripherals::I2C0,
};
use shared_bus::{I2cProxy, XtensaMutex};

static CAPTOUCH_CHANNEL: PubSubChannel<CriticalSectionRawMutex, Flow3rCaptouchEvent, 32, 32, 32> =
    PubSubChannel::<CriticalSectionRawMutex, Flow3rCaptouchEvent, 32, 32, 32>::new();

pub type CaptouchEventListener =
    Subscriber<'static, CriticalSectionRawMutex, Flow3rCaptouchEvent, 32, 32, 32>;

static PETALS: Mutex<CriticalSectionRawMutex, [Flow3rPetal; 10]> = Mutex::new([
    Flow3rPetal::new(0, Flow3rCaptouchController::TOP),
    Flow3rPetal::new(1, Flow3rCaptouchController::BOTTOM),
    Flow3rPetal::new(2, Flow3rCaptouchController::TOP),
    Flow3rPetal::new(3, Flow3rCaptouchController::BOTTOM),
    Flow3rPetal::new(4, Flow3rCaptouchController::TOP),
    Flow3rPetal::new(5, Flow3rCaptouchController::BOTTOM),
    Flow3rPetal::new(6, Flow3rCaptouchController::TOP),
    Flow3rPetal::new(7, Flow3rCaptouchController::BOTTOM),
    Flow3rPetal::new(8, Flow3rCaptouchController::TOP),
    Flow3rPetal::new(9, Flow3rCaptouchController::BOTTOM),
]);

#[derive(Debug, Clone, Copy)]
struct Flow3rCaptouchEvent {
    petals: [Flow3rPetal; 5],
}

#[derive(Debug, Clone, Copy)]
pub enum Flow3rPetal {
    TOP {
        number: u8,
        ccw: Flow3rPetalPart,
        base: Flow3rPetalPart,
        cw: Flow3rPetalPart,
    },
    BOTTOM {
        number: u8,
        base: Flow3rPetalPart,
        tip: Flow3rPetalPart,
    },
}

impl Flow3rPetal {
    const fn new(num: u8, pos: Flow3rCaptouchController) -> Flow3rPetal {
        match pos {
            Flow3rCaptouchController::TOP => Self::TOP {
                number: num,
                ccw: Flow3rPetalPart {
                    pressed: false,
                    raw: 0,
                },
                base: Flow3rPetalPart {
                    pressed: false,
                    raw: 0,
                },
                cw: Flow3rPetalPart {
                    pressed: false,
                    raw: 0,
                },
            },
            Flow3rCaptouchController::BOTTOM => Self::BOTTOM {
                number: num,
                base: Flow3rPetalPart {
                    pressed: false,
                    raw: 0,
                },
                tip: Flow3rPetalPart {
                    pressed: false,
                    raw: 0,
                },
            },
        }
    }

    pub fn pressed(&self) -> bool {
        match self {
            Flow3rPetal::TOP {
                number,
                ccw,
                base,
                cw,
            } => ccw.pressed | base.pressed | cw.pressed,
            Flow3rPetal::BOTTOM { number, base, tip } => base.pressed | tip.pressed,
        }
    }

    pub fn position(&self) -> i32 {
        match self {
            Flow3rPetal::TOP {
                number,
                ccw,
                base,
                cw,
            } => (ccw.raw as i32 + cw.raw as i32) / 2 - base.raw as i32,
            Flow3rPetal::BOTTOM { number, base, tip } => base.raw as i32 + tip.raw as i32 / 2,
        }
    }

    pub fn number(&self) -> u8 {
        match self {
            Flow3rPetal::TOP {
                number,
                ccw,
                base,
                cw,
            } => *number,
            Flow3rPetal::BOTTOM { number, base, tip } => *number,
        }
    }

    fn update(&mut self, pressed: &[bool], values: &[u16]) {
        match self {
            Flow3rPetal::TOP {
                number,
                ccw,
                base,
                cw,
            } => ccw.pressed = pressed[0],
            Flow3rPetal::BOTTOM { number, base, tip } => todo!(),
        }
    }
}

#[derive(Debug, Default, Clone, Copy)]
struct Flow3rPetalPart {
    pressed: bool,
    raw: u16,
}

#[derive(Debug, Clone, Copy)]
enum Flow3rCaptouchController {
    BOTTOM,
    TOP,
}

pub struct CaptouchHandler;

impl CaptouchHandler {
    pub fn top_petals(&self) -> TopPetals {
        TopPetals {
            petal0: Petal::new(0),
            petal2: Petal::new(2),
            petal4: Petal::new(4),
            petal6: Petal::new(6),
            petal8: Petal::new(8),
        }
    }

    pub fn bottom_petals(&self) -> BottomPetals {
        BottomPetals {
            petal1: Petal::new(1),
            petal3: Petal::new(3),
            petal5: Petal::new(5),
            petal7: Petal::new(7),
            petal9: Petal::new(9),
        }
    }
}

pub struct Petal {
    number: usize,
}

impl Petal {
    fn new(number: usize) -> Self {
        Self { number }
    }

    pub async fn pressed(&self) -> bool {
        let fp = PETALS.lock().await[self.number];
        fp.pressed()
    }

    pub async fn position(&self) -> i32 {
        let fp = PETALS.lock().await[self.number];
        fp.position()
    }
}

pub struct TopPetals {
    pub petal0: Petal,
    pub petal2: Petal,
    pub petal4: Petal,
    pub petal6: Petal,
    pub petal8: Petal,
}

pub struct BottomPetals {
    pub petal1: Petal,
    pub petal3: Petal,
    pub petal5: Petal,
    pub petal7: Petal,
    pub petal9: Petal,
}

fn init_captouch(
    i2c_bot: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>,
    i2c_top: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>,
) -> Result<
    (
        Ad7147<I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>, Initialized, 12>,
        Ad7147<I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>, Initialized, 12>,
    ),
    hal::i2c::Error,
> {
    let ad7147_bot = Ad7147::new(i2c_bot, 0b00101101);
    let config_bot = DeviceConfiguration::builder()
        .decimation(DecimationFactor::Factor64)
        .stages([
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(2)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN0,
                    cdc: CdcInput::Positive,
                })
                .sensitivity(StageSensitivity {
                    neg_threshold_sensitivity: ThresholdSensitivity::Percent90,
                    neg_peak_detect: PeakDetect::Percent90,
                    pos_threshold_sensitivity: ThresholdSensitivity::Percent90,
                    pos_peak_detect:  PeakDetect::Percent90,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(2)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN1,
                    cdc: CdcInput::Positive,
                })
                .sensitivity(StageSensitivity {
                    neg_threshold_sensitivity: ThresholdSensitivity::Percent90,
                    neg_peak_detect: PeakDetect::Percent90,
                    pos_threshold_sensitivity: ThresholdSensitivity::Percent90,
                    pos_peak_detect:  PeakDetect::Percent90,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(2)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN2,
                    cdc: CdcInput::Positive,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(2)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN3,
                    cdc: CdcInput::Positive,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(2)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN4,
                    cdc: CdcInput::Positive,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(2)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN5,
                    cdc: CdcInput::Positive,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(2)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN6,
                    cdc: CdcInput::Positive,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(2)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN7,
                    cdc: CdcInput::Positive,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(2)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN8,
                    cdc: CdcInput::Positive,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(2)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN9,
                    cdc: CdcInput::Positive,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(30)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN10,
                    cdc: CdcInput::Positive,
                })
                .initial_offset_high(55000)
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .conversion_complete_interrupt_enabled(true)
                .pos_afe_offset(26)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN11,
                    cdc: CdcInput::Positive,
                })
                .initial_offset_high(55000)
                .build(),
        ])
        .build();
    let ad7147_top = Ad7147::new(i2c_top, 0b00101100);
    let config_top = DeviceConfiguration::builder()
        .decimation(DecimationFactor::Factor64)
        .stages([
            StageConfiguration::builder()
                .calibration_enabled(true)
                .conversion_complete_interrupt_enabled(true)
                .pos_afe_offset(30)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN0,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(30)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN1,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(28)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN2,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(28)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN3,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(28)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN4,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(28)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN5,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(28)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN6,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(10)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN7,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(20)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN8,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .pos_afe_offset(20)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN9,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .conversion_complete_interrupt_enabled(false)
                .pos_afe_offset(8)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN10,
                    cdc: CdcInput::Positive,
                })
                .build(),
            StageConfiguration::builder()
                .calibration_enabled(true)
                .conversion_complete_interrupt_enabled(true)
                .pos_afe_offset(20)
                .add_input_connection(InputConnection {
                    cin: CapInput::CIN11,
                    cdc: CdcInput::Positive,
                })
                .build(),
        ])
        .build();
    Ok((
        ad7147_bot.init(config_bot, &mut Delay)?,
        ad7147_top.init(config_top, &mut Delay)?,
    ))
}

#[embassy_executor::task]
pub async fn captouch_controller(
    i2c_bot: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>,
    i2c_top: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>,
    cap_bot_int: Gpio16<Unknown>,
    cap_top_int: Gpio15<Unknown>,
) -> ! {
    let mut cap_bot_int = cap_bot_int.into_pull_up_input();
    let mut cap_top_int = cap_top_int.into_pull_up_input();
    let (mut ad7147_bot, mut ad7147_top) = init_captouch(i2c_bot, i2c_top).unwrap();

    let device_id = ad7147_bot.read_device_id().unwrap();
    println!("captouch bot device id: {:016b}", device_id);
    let device_id = ad7147_top.read_device_id().unwrap();
    println!("captouch top device id: {:016b}", device_id);
    println!("initialized captouch");
    loop {
        match select(cap_bot_int.wait_for_low(), cap_top_int.wait_for_low()).await {
            Either::First(_) => {
                let interrupts = ad7147_bot.read_interrupt_registers().unwrap();
                let measurements_bot = ad7147_bot.read_all_stages().unwrap();
                // println!("pad 0: {}, {}, {}, {}", measurements_bot[0], measurements_bot[1], interrupts[1] & (1 << 0) != 0, interrupts[1] & (1 << 1) != 0);
                update_petals_bot(interrupts[1], measurements_bot).await;
            }
            Either::Second(_) => {
                let interrupts = ad7147_top.read_interrupt_registers().unwrap();
                let measurements_top = ad7147_top.read_all_stages().unwrap();
                update_petals_top(interrupts[1], measurements_top).await;
            }
        }
    }
}

#[derive(Debug, Copy, Clone)]
enum PetalPosition {
    CCW,
    BASE,
    CW,
    TIP,
}

static PETAL_MAPPING_TOP: [(usize, PetalPosition); 12] = [
    (0, PetalPosition::BASE),
    (0, PetalPosition::CCW),
    (0, PetalPosition::CW),
    (8, PetalPosition::CW),
    (8, PetalPosition::CCW),
    (8, PetalPosition::BASE),
    (4, PetalPosition::BASE),
    (4, PetalPosition::CCW),
    (4, PetalPosition::CW),
    (6, PetalPosition::CW),
    (6, PetalPosition::CCW),
    (6, PetalPosition::BASE),
];

async fn update_petals_top(interrupts: u16, measurements: [u16; 12]) {
    let mut petals = PETALS.lock().await;
    for (i, m) in PETAL_MAPPING_TOP.iter().enumerate() {
        let pressed = measurements[i] > 50000;
        match petals[m.0] {
            Flow3rPetal::TOP {
                ref mut ccw,
                ref mut base,
                ref mut cw,
                ..
            } => match m.1 {
                PetalPosition::CCW => {
                    ccw.pressed = pressed;
                    ccw.raw = measurements[i]
                }
                PetalPosition::BASE => {
                    base.pressed = pressed;
                    base.raw = measurements[i]
                }
                PetalPosition::CW => {
                    cw.pressed = pressed;
                    cw.raw = measurements[i]
                }
                PetalPosition::TIP => panic!("top petal does not have tip"),
            },
            Flow3rPetal::BOTTOM {
                ref mut base,
                ref mut tip,
                ..
            } => match m.1 {
                PetalPosition::CCW => panic!("bottom petal does not have ccw"),
                PetalPosition::BASE => {
                    base.pressed = pressed;
                    base.raw = measurements[i]
                }
                PetalPosition::CW => panic!("bottom petal does not have cw"),
                PetalPosition::TIP => {
                    tip.pressed = pressed;
                    tip.raw = measurements[i]
                }
            },
        }
    }
}

static PETAL_MAPPPING_BOT: [(usize, PetalPosition); 12] = [
    (9, PetalPosition::BASE),
    (9, PetalPosition::TIP),
    (7, PetalPosition::BASE),
    (7, PetalPosition::TIP),
    (5, PetalPosition::BASE),
    (5, PetalPosition::TIP),
    (3, PetalPosition::BASE),
    (3, PetalPosition::TIP),
    (1, PetalPosition::BASE),
    (1, PetalPosition::TIP),
    (2, PetalPosition::BASE),
    (2, PetalPosition::CW),
];

async fn update_petals_bot(interrupts: u16, measurements: [u16; 12]) {
    let mut petals = PETALS.lock().await;
    for (i, m) in PETAL_MAPPPING_BOT.iter().enumerate() {
        let pressed = measurements[i] > 40000;
        match petals[m.0] {
            Flow3rPetal::TOP {
                ref mut ccw,
                ref mut base,
                ref mut cw,
                ..
            } => match m.1 {
                PetalPosition::CCW => {
                    ccw.pressed = pressed;
                    ccw.raw = measurements[i]
                }
                PetalPosition::BASE => {
                    base.pressed = pressed;
                    base.raw = measurements[i]
                }
                PetalPosition::CW => {
                    cw.pressed = pressed;
                    cw.raw = measurements[i]
                }
                PetalPosition::TIP => panic!("top petal does not have tip"),
            },
            Flow3rPetal::BOTTOM {
                ref mut base,
                ref mut tip,
                ..
            } => match m.1 {
                PetalPosition::CCW => panic!("bottom petal does not have ccw"),
                PetalPosition::BASE => {
                    base.pressed = pressed;
                    base.raw = measurements[i]
                }
                PetalPosition::CW => panic!("bottom petal does not have cw"),
                PetalPosition::TIP => {
                    tip.pressed = pressed;
                    tip.raw = measurements[i]
                }
            },
        }
    }
}
