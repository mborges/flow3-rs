use esp_hal_smartled::{smartLedAdapter, SmartLedsAdapter};
use hal::{
    gpio::{Gpio14, GpioPin, Unknown},
    pulse_control::{Channel0, ConfiguredChannel0},
};

pub type Leds = SmartLedsAdapter<ConfiguredChannel0<'static, GpioPin<Unknown, 14>>, 961>;

pub fn init_leds(channel: Channel0, pin: Gpio14<Unknown>) -> Leds {
    <smartLedAdapter!(40)>::new(channel, pin)
}

pub fn brightness_fade_in_out(brightness: (u8, bool), max: u8, min: u8) -> (u8, bool) {
    match brightness {
        (v, true) if v < max => (v + 1, true),
        (v, true) => (v - 1, false),
        (v, false) if v > min => (v - 1, false),
        (v, false) => (v + 1, true),
    }
}
