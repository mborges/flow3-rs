use embassy_futures::select::{select, select3, Either, Either3};
use embassy_sync::{
    blocking_mutex::raw::CriticalSectionRawMutex,
    pubsub::{PubSubChannel, Subscriber, WaitResult},
};
use embedded_hal_async::digital::Wait;
use esp_println::println;
use hal::{
    gpio::{Gpio0, Gpio3, Gpio8, Unknown},
    i2c::I2C,
    peripherals::I2C0,
};
use shared_bus::{I2cProxy, XtensaMutex};

static INPUTS_CHANNEL: PubSubChannel<CriticalSectionRawMutex, Flow3rInputEvent, 32, 32, 32> =
    PubSubChannel::<CriticalSectionRawMutex, Flow3rInputEvent, 32, 32, 32>::new();

pub type InputEventListener =
    Subscriber<'static, CriticalSectionRawMutex, Flow3rInputEvent, 32, 32, 32>;

pub struct InputHandler;

impl InputHandler {
    pub fn get_event_listener(
        &mut self,
    ) -> Result<InputEventListener, embassy_sync::pubsub::Error> {
        INPUTS_CHANNEL.subscriber()
    }

    pub fn split(self) -> InputParts {
        InputParts {
            sw1_left: Flow3rInput(FlowerInputEventSource::SW1Left),
            sw1_center: Flow3rInput(FlowerInputEventSource::SW1Press),
            sw1_right: Flow3rInput(FlowerInputEventSource::SW1Right),
            sw2_left: Flow3rInput(FlowerInputEventSource::SW2Left),
            sw2_center: Flow3rInput(FlowerInputEventSource::SW2Press),
            sw2_right: Flow3rInput(FlowerInputEventSource::SW2Right),
        }
    }
}

async fn wait_for_event(
    input: FlowerInputEventSource,
    etype: Flow3rInputEventType,
) -> Result<(), embassy_sync::pubsub::Error> {
    let mut sub = INPUTS_CHANNEL.subscriber()?;
    loop {
        match sub.next_message().await {
            WaitResult::Lagged(_) => (),
            WaitResult::Message(msg) => {
                if msg == (Flow3rInputEvent { input, etype }) {
                    return Ok(());
                }
            }
        }
    }
}

pub struct InputParts {
    pub sw1_left: Flow3rInput,
    pub sw1_center: Flow3rInput,
    pub sw1_right: Flow3rInput,
    pub sw2_left: Flow3rInput,
    pub sw2_center: Flow3rInput,
    pub sw2_right: Flow3rInput,
}

pub struct Flow3rInput(FlowerInputEventSource);

impl Flow3rInput {
    pub async fn wait_for_release(&mut self) -> Result<(), embassy_sync::pubsub::Error> {
        wait_for_event(self.0, Flow3rInputEventType::Press).await
    }

    pub async fn wait_for_press(&mut self) -> Result<(), embassy_sync::pubsub::Error> {
        wait_for_event(self.0, Flow3rInputEventType::Press).await
    }

    pub async fn wait_for_any(&mut self) -> Result<(), embassy_sync::pubsub::Error> {
        match select(
            wait_for_event(self.0, Flow3rInputEventType::Press),
            wait_for_event(self.0, Flow3rInputEventType::Release),
        )
        .await
        {
            Either::First(e) => e,
            Either::Second(e) => e,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Flow3rInputEvent {
    input: FlowerInputEventSource,
    etype: Flow3rInputEventType,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum FlowerInputEventSource {
    SW1Left,
    SW1Press,
    SW1Right,
    SW2Left,
    SW2Press,
    SW2Right,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Flow3rInputEventType {
    Press,
    Release,
}

#[embassy_executor::task]
pub async fn input_controller(
    i2c: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>,
    i2c_int: Gpio8<Unknown>,
    sw1_button: Gpio0<Unknown>,
    sw2_button: Gpio3<Unknown>,
) {
    let mut i2c_int = i2c_int.into_pull_up_input();
    let mut sw1_button = sw1_button.into_pull_up_input();
    let mut sw2_button = sw2_button.into_pull_up_input();

    let mut pe2 = port_expander::Max7321::new(i2c, true, true, false, true);

    let pe2_io = pe2.split();
    let mut sw1_r = pe2_io.p0;
    sw1_r.set_high().unwrap();
    let mut sw1_l = pe2_io.p7;
    sw1_l.set_high().unwrap();
    let mut sw2_r = pe2_io.p5;
    sw2_r.set_high().unwrap();
    let mut sw2_l = pe2_io.p4;
    sw2_l.set_high().unwrap();

    let _lcd_rst = pe2_io.p3;

    let inputs_publisher = INPUTS_CHANNEL.immediate_publisher();

    loop {
        match select3(
            i2c_int.wait_for_low(),
            sw1_button.wait_for_any_edge(),
            sw2_button.wait_for_any_edge(),
        )
        .await
        {
            Either3::First(_) => {
                let values =
                    port_expander::read_multiple([&sw1_l, &sw1_r, &sw2_l, &sw2_r]).unwrap();
                if !values[0] {
                    inputs_publisher.publish_immediate(Flow3rInputEvent {
                        input: FlowerInputEventSource::SW1Left,
                        etype: Flow3rInputEventType::Press,
                    });
                }
                if !values[1] {
                    inputs_publisher.publish_immediate(Flow3rInputEvent {
                        input: FlowerInputEventSource::SW1Right,
                        etype: Flow3rInputEventType::Press,
                    });
                }
                if !values[2] {
                    inputs_publisher.publish_immediate(Flow3rInputEvent {
                        input: FlowerInputEventSource::SW2Left,
                        etype: Flow3rInputEventType::Press,
                    });
                }
                if !values[3] {
                    inputs_publisher.publish_immediate(Flow3rInputEvent {
                        input: FlowerInputEventSource::SW2Right,
                        etype: Flow3rInputEventType::Press,
                    });
                }
            }
            Either3::Second(_) => inputs_publisher.publish_immediate(Flow3rInputEvent {
                input: FlowerInputEventSource::SW1Press,
                etype: Flow3rInputEventType::Press,
            }),
            Either3::Third(_) => inputs_publisher.publish_immediate(Flow3rInputEvent {
                input: FlowerInputEventSource::SW2Press,
                etype: Flow3rInputEventType::Press,
            }),
        }
    }
}

#[embassy_executor::task]
async fn input_logger() -> ! {
    let mut inputs_subscriber = INPUTS_CHANNEL.subscriber().unwrap();
    loop {
        let message = inputs_subscriber.next_message().await;
        match message {
            WaitResult::Message(msg) => println!("input {:?} activated", msg.input),
            WaitResult::Lagged(err) => println!("input logger lagged: {}", err),
        }
    }
}
